/*Task 1: 
1. Write a JavaScript script that declares two variables, `num1` and `num2`, and assigns them numeric values.
2. Perform basic arithmetic operations (addition, subtraction, multiplication, and division) on these variables.
3. Print the results to the console with meaningful messages.*/

var num1=10, num2=15;
console.log("10 plus 15 equals to "+ (num1+num2))
console.log("10 minus 15 equals to "+ (num1-num2))
console.log("10 times 15 equals to "+ (num1*num2))
console.log("10 divide 15 equals to "+ (num1/num2))